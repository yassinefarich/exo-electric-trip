# this exam
Forked from repo : https://bitbucket.org/gjambet/exo-electric-trip.git

Main test : https://gitlab.com/yassinefarich/exo-electric-trip/blob/master/src/test/java/io/trefles/exercise/electric_trip/ElectricTripTest.java

# if you are taking the exam

1. Please Fork the repo, work on your own fork.
2. Difficulty is increasing, solve [test](https://gitlab.com/yassinefarich/exo-electric-trip/blob/master/src/test/java/io/trefles/exercise/electric_trip/ElectricTripTest.java) one by one, we want to see your method not the overall solution magically jump out of the hat.
3. Do commit & push each time a new test becomes green.
4. Keep in mind that this is a session to assess your coding skills : how you are doing stuff is as important as passing all the tests.

:warning:**You're not allowed to change a single character of the tests**

Good luck :smiley:



# if you are benching the exercise

up to you, but feel free to create your own branch to host your solution and discuss it with team mates
please commit somewhere or tell me how long it took you to perform

Carlo (copy-pasted from Guillaume)